let couseName = document.querySelector('#add-course-name')
let couseDesc = document.querySelector('#add-course-desc')
let cousePrice = document.querySelector('#add-course-price')
let token = localStorage.getItem('accessToken')
console.log(token)

document.querySelector("#form-addCourse").addEventListener('submit', (e) =>{

	e.preventDefault()

	fetch('http://localhost:4000/courses/', {

		method: 'POST', 
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}, 
		body: JSON.stringify({

			name: couseName.value,
			description: couseDesc.value,
			price: cousePrice.value

		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
	})

})