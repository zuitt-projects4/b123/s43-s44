/*
	get users details
	localStorage.getItem(<key>) - this will return the value of the key from our local storage.
*/
let profileName = document.querySelector('#profile-name')
let profileEmail = document.querySelector('#profile-email')
let profileMobile = document.querySelector('#profile-mobile')
let token = localStorage.getItem('accessToken')
console.log(token)

fetch('http://localhost:4000/users/getUserDetails', {

	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data)

	profileName.innerHTML = `${data.firstName} ${data.lastName}`
	profileEmail.innerHTML = `${data.email}`
	profileMobile.innerHTML = `${data.mobileNo}`

})