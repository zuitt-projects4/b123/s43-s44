let firstNameRegister = document.querySelector("#first-name-register")
let lastNameRegister = document.querySelector("#last-name-register")
let mobileNoRegister = document.querySelector("#mobile-num-register")
let emailRegister = document.querySelector("#email-register")
let passwordRegister = document.querySelector("#password-register")

document.querySelector("#form-register").addEventListener('submit', (e) =>{

	e.preventDefault()

	console.log(firstNameRegister.value)
	console.log(lastNameRegister.value)
	console.log(mobileNoRegister.value)
	console.log(emailRegister.value)
	console.log(passwordRegister.value)

	fetch('http://localhost:4000/users/', {

		method: 'POST', 
		headers: {
			'Content-Type': 'application/json'
		}, 
		body: JSON.stringify({

			firstName: firstNameRegister.value, 
			lastName: lastNameRegister.value, 
			mobileNo: mobileNoRegister.value, 
			email: emailRegister.value, 
			password: passwordRegister.value, 

		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
	})

})

/*
	
	can send these values to our api
	GET method requests only would need the URL or headers for tokens.
	However other requests with a different HTTP method, we would have to explicitly declare: 
	Options is objects which will  contain:
		headers: authorization token or content type headers
		HTTP method
		body of our request

	syntax: fetch(<requestURL>, {options})
		
*/