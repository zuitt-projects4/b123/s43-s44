let emailLogin = document.querySelector("#email-login")
let passwordLogin = document.querySelector("#password-login")

document.querySelector("#form-login").addEventListener('submit', (e)=>{

	e.preventDefault()

	console.log(emailLogin.value)
	console.log(passwordLogin.value)
	fetch('http://localhost:4000/users/login', {

		method: 'POST', 
		headers: {
			'Content-Type': 'application/json'
		}, 
		body: JSON.stringify({

			email: emailLogin.value, 
			password: passwordLogin.value, 

		})
	})
	.then(res => res.json())
	.then(data => {
		/*console.log(data)*/
		localStorage.setItem('accessToken', data.accessToken)
	})
})

/*
	localStorage - is an object in JS which will allows us to save small amounts of data within our browser. We can use this to save our token. local storage exists in most modern browsers.

	localStorage.setItem() will allow us to save data in our browsers. However, any data that we pass to our localStorage will become a string.

	syntax: localStorage.setItem(<key>, <value>)

*/

