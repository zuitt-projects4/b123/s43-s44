/*
	innerHTML - is a property of an element which contains all of the children(other elements or text content ) as a string

*/
let coursesDiv = document.querySelector("#courses-div")
console.log(coursesDiv)


/*
let mainDiv = document.querySelector("#main-div")

console.log(mainDiv.innerHTML)

mainDiv.innerHTML = "<h1>Batch 123 is awesome</h1>"
mainDiv.innerHTML += "<p> Full Stack Developer</p>"
mainDiv.innerHTML = mainDiv.innerHTML + "<p>I am Louise</p>"
*/
/*
	fetch() - is a js method which allow us to pass or create a request to an api.

	syntax: fetch(<requestURL>)

	.then() - allows us to handle/process the result of a previous function

	.then(res => res.json())- it handles/process the server's response and turns the response into a proper JS object.

	.then(data => {}) -  res => res.json() 
*/
fetch('http://localhost:4000/courses/getActiveCourses')
.then(res => res.json())
.then(data => {

	let courseCards = ""
	data.forEach((course)=>{
		courseCards += `
			<div class="card">
				<h4>${course.name}</h4>
				<p>${course.description}</p>
				<span> Price: PHP ${course.price}</span>
			</div>

		`
	})

	coursesDiv.innerHTML = courseCards

})